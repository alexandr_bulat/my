package com.example.abulat.fg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Mainlist extends AppCompatActivity {
    Button listbtn;
    Button gridtbtn;
    Button tablebtn;
    Button recbtn;
    Button allone;
    MyDataBase mdb;
    private static final String DATABASE_NAME = "ImageDb.db";
    public static final int DATABASE_VERSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_list);
        listbtn = (Button) findViewById(R.id.listbtn);
        allone = (Button) findViewById(R.id.allone);

        gridtbtn = (Button) findViewById(R.id.gridbtn);
        tablebtn = (Button) findViewById(R.id.tablebtn);
        recbtn = (Button) findViewById(R.id.recbtn);
        mdb = new MyDataBase(getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
        listbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mainlist.this, MainActivity.class);
                startActivity(intent);
            }
        });
        gridtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mainlist.this, Gridlist.class);
                startActivity(intent);

            }
        });
        tablebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mainlist.this, TableView.class);
                startActivity(intent);

            }
        });
        recbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mainlist.this, Recyclview.class);
                startActivity(intent);

            }
        });
        allone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Mainlist.this, Allone.class);
                startActivity(intent);

            }
        });
    }

}
