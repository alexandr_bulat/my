package com.example.abulat.fg;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.abulat.fg.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.example.abulat.fg.Contacts;
import com.example.abulat.fg.MyDataBase;

public class FragmentList extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ListView listview;
    Button setbtn;
    int curren_img = 0;
    StringBuffer buffer;
    Button btn;
    private MyDataBase mdb = null;
    EditText edname;
    private SQLiteDatabase db = null;
    private Cursor c = null;
    private byte[] img = null;
    private static final String DATABASE_NAME = "ImageDb12.db";
    public static final int DATABASE_VERSION = 1;
    String[] mas1 = {"Arc", "Fil", "Alex", "Dima"};
    Contacts contacts;
    List<Contacts> contactses = new ArrayList<>();
    Bitmap b1;
    Bitmap b;

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);



    }

    public void setimagetext() {
        setbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] mas2 = {R.drawable.arubailo, R.drawable.fil, R.drawable.b, R.drawable.me};

                curren_img++;
                curren_img = curren_img % mas2.length;


                b = BitmapFactory.decodeResource(getResources(), mas2[curren_img]);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, bos);
                img = bos.toByteArray();
                db = mdb.getWritableDatabase();
                ContentValues cv = new ContentValues();

                cv.put("image", img);
                cv.put("NAME", edname.getText().toString());

                db.insert("tableimage", null, cv);

                edname.setText("");

            }
        });


    }

    public void showuser() {

        String[] col = {"image"};

        db = mdb.getReadableDatabase();
        c = db.rawQuery("select * from tableimage", null);
        buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("Name:    " + c.getString(0) + "\n");
            contacts = new Contacts();
            contacts.setName(c.getString(0));
            img = c.getBlob(c.getColumnIndex("image"));
            b1 = BitmapFactory.decodeByteArray(img, 0, img.length);
            contacts.setImage(b1);
            contactses.add(contacts);
            listview.setAdapter(new TestAdapter(getActivity(), contactses));
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_main, container, false);
        edname = (EditText) v.findViewById(R.id.edname);
        mdb = new MyDataBase(getActivity(), DATABASE_NAME, null, DATABASE_VERSION);
        btn = (Button) v.findViewById(R.id.btn);
        setbtn = (Button) v.findViewById(R.id.setnameimage);
        listview = (ListView) v.findViewById(R.id.listView);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showuser();


            }
        });
        setbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setimagetext();

            }
        });

        return v;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        item.setChecked(true);

        if (id == R.id.item1) {


            mListener.onFragmentInteraction(null);

            contactses.add(new Contacts("POP", b1));


            listview.setAdapter(new TestAdapter(getActivity(), contactses));


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class TestAdapter extends BaseAdapter {

        List<Contacts> listItem;
        Context mContext;

        public TestAdapter(Context mContext, List<Contacts> listItem) {
            this.mContext = mContext;
            this.listItem = listItem;

        }

        public int getCount() {
            return listItem.size();
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View arg1, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.row_edit, viewGroup, false);


            final TextView bodytext = (TextView) row.findViewById(R.id.textView2);
            final ImageView imageView = (ImageView) row.findViewById(R.id.imageView);
            Button delbtn = (Button) row.findViewById(R.id.delbtn);

            final Contacts contacts1 = listItem.get(position);
            delbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listItem.remove(position);
                    notifyDataSetChanged();

                    db = mdb.getWritableDatabase();
                    db.delete("tableimage", "NAME= ?", new String[]{contacts1.getName()});

                }
            });

            imageView.setImageBitmap(contacts1.getImage());

            bodytext.setText(contacts1.getName());

            return row;
        }
    }
}
