package com.example.abulat.fg;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by abulat on 7/7/16.
 */
public class Contacts {


    private String name;
    private Bitmap image;


    public Contacts() {

    }

    public Contacts(String name, Bitmap image) {

        this.name = name;
        this.image = image;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

}
