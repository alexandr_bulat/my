package com.example.abulat.fg;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ListView listview;
    Button setbtn;
    int curren_img = 0;
    StringBuffer buffer;
    Button btn;
    private MyDataBase mdb = null;
    EditText edname;
    private SQLiteDatabase db = null;
    private Cursor c = null;
    private byte[] img = null;
    private static final String DATABASE_NAME = "ImageDb12.db";
    public static final int DATABASE_VERSION = 1;

    String[] mas1 = {"Arc", "Fil", "Alex", "Dima"};

    Contacts contacts;
    List<Contacts> contactses = new ArrayList<>();
    ;
    int getRandom;
    Bitmap b1;
    Bitmap b;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edname = (EditText) findViewById(R.id.edname);
        mdb = new MyDataBase(getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
        btn = (Button) findViewById(R.id.btn);
        setbtn = (Button) findViewById(R.id.setnameimage);


        listview = (ListView) findViewById(R.id.listView);
        generan();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowUser();


            }
        });
        setbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetImagetext();

            }
        });


    }

    int generan() {
        Random r = new Random();
        getRandom = r.nextInt(mas1.length);

        return getRandom;
    }

    public void SetImagetext() {
        int[] mas2 = {R.drawable.arubailo, R.drawable.fil, R.drawable.b, R.drawable.me};

        curren_img++;
        curren_img = curren_img % mas2.length;


        b = BitmapFactory.decodeResource(getResources(), mas2[curren_img]);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
        img = bos.toByteArray();
        db = mdb.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("image", img);
        cv.put("NAME", edname.getText().toString());

        db.insert("tableimage", null, cv);

        edname.setText("");
        db.close();

    }

    public void ShowUser() {

        String[] col = {"image"};

        db = mdb.getReadableDatabase();
        c = db.rawQuery("select * from tableimage", null);

        buffer = new StringBuffer();

        while (c.moveToNext()) {
            buffer.append("Name:    " + c.getString(0) + "\n");
            contacts = new Contacts();
            contacts.setName(c.getString(0));
            img = c.getBlob(c.getColumnIndex("image"));
            b1 = BitmapFactory.decodeByteArray(img, 0, img.length);
            contacts.setImage(b1);
            contactses.add(contacts);


            listview.setAdapter(new TestAdapter(this, contactses));
        }

        listview.setAdapter(new TestAdapter(this, contactses));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                generan();
                contacts.setName(edname.getText().toString());
                contactses.add(contacts);
                listview.setAdapter(new TestAdapter(this, contactses));


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class TestAdapter extends BaseAdapter {

        List<Contacts> listItem;

        Context mContext;

        public TestAdapter(Context mContext, List<Contacts> listItem) {
            this.mContext = mContext;
            this.listItem = listItem;

        }

        public int getCount() {
            return listItem.size();
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View arg1, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.row_edit, viewGroup, false);


            TextView addresstext = (TextView) row.findViewById(R.id.textView);
            final TextView bodytext = (TextView) row.findViewById(R.id.textView2);
            final ImageView imageView = (ImageView) row.findViewById(R.id.imageView);
            Button delbtn = (Button) row.findViewById(R.id.delbtn);

            final Contacts contacts1 = listItem.get(position);
            delbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listItem.remove(position);
                    notifyDataSetChanged();

                    db = mdb.getWritableDatabase();
                    db.delete("tableimage", "NAME= ?", new String[]{contacts1.getName()});
                    db = mdb.getWritableDatabase();
                    db.delete("tableimage", "NAME= ?", new String[]{contacts1.getName()});
                    db.close();
                }
            });

            imageView.setImageBitmap(contacts1.getImage());

            bodytext.setText(contacts1.getName());

            return row;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();

    }
}



