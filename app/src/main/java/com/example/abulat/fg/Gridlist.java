package com.example.abulat.fg;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Gridlist extends AppCompatActivity {

    Contacts1 contacts;
    int curren_img = 0;
    GridView gridview;
    TestAdapter adapter;

    ArrayList<Contacts1> contactses;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridlist);


        gridview = (GridView) findViewById(R.id.gridView);


        contactses = new ArrayList<Contacts1>();
        contactses.add(new Contacts1(R.drawable.arubailo));
        contactses.add(new Contacts1(R.drawable.arubailo));
        contactses.add(new Contacts1(R.drawable.arubailo));
        contactses.add(new Contacts1(R.drawable.arubailo));
        adapter = new TestAdapter(this, contactses);
        gridview.setAdapter(adapter);
        gridview.setNumColumns(4);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                contactses.add(new Contacts1(R.drawable.arubailo));
                adapter.notifyDataSetChanged();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class TestAdapter extends BaseAdapter {

        private Context mContext;
        List<Contacts1> listItem;

        public TestAdapter(Context c, ArrayList<Contacts1> listItem) {
            mContext = c;
            this.listItem = listItem;
        }

        @Override
        public int getCount() {
            return listItem.size();
        }

        @Override
        public Object getItem(int arg0) {
            return listItem.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View grid;
            if (convertView == null) {

                grid = new View(mContext);
                LayoutInflater inflater = getLayoutInflater();
                grid = inflater.inflate(R.layout.row_image, parent, false);
            } else {
                grid = (View) convertView;
            }


            ImageView imageView = (ImageView) grid.findViewById(R.id.image);
            imageView.setImageResource(listItem.get(position).getImage());


            return grid;

        }

    }
}

