package com.example.abulat.fg;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import arraylist.Contacts1;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentGrid.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentGrid#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentGrid extends Fragment {
    Contacts1 contacts;
    GridView gridview;
    List<Contacts1> contactses = new ArrayList<>();
    ;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private Allone allone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_gridlist, container, false);

        contactses.add(new Contacts1(R.drawable.arubailo));
        gridview = (GridView) v.findViewById(R.id.gridView);
        gridview.setAdapter(new TestAdapter(getActivity(), contactses));
        gridview.setNumColumns(4);
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            allone = (Allone) context;


        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        item.setChecked(true);
        if (id == R.id.item1) {

            mListener.onFragmentInteraction(null);

            contactses.add(new Contacts1(R.drawable.arubailo));
            gridview.setAdapter(new TestAdapter(getActivity(), contactses));


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class TestAdapter extends BaseAdapter {

        private Context mContext;
        List<Contacts1> contactses;

        public TestAdapter(Context c, List<Contacts1> list) {
            mContext = c;
            this.contactses = list;
        }

        @Override
        public int getCount() {
            return contactses.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View grid;
            Contacts1 contacts = contactses.get(position);

            if (convertView == null) {
                grid = new View(mContext);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                grid = inflater.inflate(R.layout.row_image, parent, false);
            } else {
                grid = (View) convertView;
            }

            ImageView imageView = (ImageView) grid.findViewById(R.id.image);
            imageView.setImageResource(contacts.getImage());

            return grid;
        }
    }
}
